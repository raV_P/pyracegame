import pygame
import time
import random

pygame.init()

display_width = 800
display_height = 600

black = (0, 0, 0)
white = (255, 255, 255)

red = (200, 0, 0)
bright_red = (255, 0, 0)
green = (0, 200, 0)
bright_green = (0, 255, 0)

block_color = (52, 23, 202)

gameDisplay = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption('My Tower Defense')
clock = pygame.time.Clock()

carImg = pygame.image.load('car.png')


def things_dodged(count):
    font = pygame.font.SysFont(None, 25)
    text = font.render("Dodged: " + str(count), True, black)
    gameDisplay.blit(text, (0, 0))


def things(x, y, w, h, color):
    pygame.draw.rect(gameDisplay, color, [x, y, w, h])


def car(x, y):
    gameDisplay.blit(carImg, (x, y))


def crash():
    message_display('You crashed')


def text_objects(text, font):
    textSurf = font.render(text, True, black)
    return textSurf, textSurf.get_rect()


def message_display(msg: str):
    largeText = pygame.font.Font('freesansbold.ttf', 24)
    TextSurf, TextRect = text_objects(msg, largeText)
    TextRect.center = (display_width / 2, display_height / 2)
    gameDisplay.blit(TextSurf, TextRect)

    pygame.display.update()

    time.sleep(4)

    game_loop()

def button(msg, x,y,w,h,inactive_color, active_color, action=None):
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    if x + w > mouse[0] > x and y + h > mouse[1] > y:
        pygame.draw.rect(gameDisplay, active_color, (x, y, w, h))
        if click[0] == 1 and action != None:
            action()

    else:
        pygame.draw.rect(gameDisplay, inactive_color, (x, y, w, h))
    smallText = pygame.font.Font("freesansbold.ttf", 20)
    textSurf, textRect = text_objects(msg, smallText)
    textRect.center = ((x + (w / 2)), (y + (h / 2)))
    gameDisplay.blit(textSurf, textRect)

def quitgame():
    pygame.quit()
    quit()

def game_intro():
    intro = True

    while intro:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        gameDisplay.fill(white)
        largeText = pygame.font.Font('freesansbold.ttf', 24)
        TextSurf, TextRect = text_objects("My race game", largeText)
        TextRect.center = (display_width / 2, display_height / 2)
        gameDisplay.blit(TextSurf, TextRect)

        # button(msg, x, y, w, h, inactive_color, active_color):
        button_go = button("GO!", 150, 450, 100, 50, green, bright_green, game_loop)
        button_go = button("Quit", 550, 450, 100, 50, red, bright_red, quitgame)


        pygame.display.update()
        clock.tick(10)


def game_loop():
    x = (display_width * 0.45)
    y = (display_height * 0.8)
    x_change = 0
    y_change = 0
    car_width = 60
    dodged = 0

    thing_startx = random.randrange(0, display_width)
    thing_starty = -100
    thing_speed = 10
    thing_w = 50
    thing_h = 50

    gameExit = False
    while not gameExit:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gameExit = True
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    x_change = -5
                if event.key == pygame.K_RIGHT:
                    x_change = 5

            if event.type == pygame.KEYUP:
                if (event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT):
                    x_change = 0

        x += x_change

        gameDisplay.fill(white)

        things(thing_startx, thing_starty, thing_w, thing_h, block_color)
        thing_starty += thing_speed

        car(x, y)
        things_dodged(dodged)

        if x > display_width - car_width or x < 0:
            crash()

        if thing_starty > display_height:
            thing_starty = 0 - thing_h
            thing_startx = random.randrange(0, display_width - thing_w)
            dodged += 1
            # thing_speed += 1
            thing_w += (dodged * 2)

        if y < thing_starty + thing_h:
            if x > thing_startx and x < thing_startx + thing_w or x + car_width > thing_startx and x + car_width < thing_startx + thing_w:
                crash()

        pygame.display.update()
        clock.tick(60)

game_intro()
game_loop()
pygame.quit()
quit()
